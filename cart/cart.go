package cart

type Cart struct {
	Items []Item
	Qty   int
	Total int
}

type Item struct {
	Name  string
	Qty   int
	Price int
	Total int
}

func (cart *Cart) AddItem(name string, qty, price int) {
	item := Item{}
	item.Name = name
	item.Qty = qty
	item.Price = price

	cart.Items = append(cart.Items, item)
	cart.recalculate()
}

func (cart *Cart) RemoveItem(index int) {
	cart.Items = append(cart.Items[:index], cart.Items[index+1:]...)

	cart.recalculate()
}

func (cart *Cart) UpdateItem(index, qty, price int) {
	item := cart.Items[index]
	item.Qty = qty
	item.Price = price

	cart.Items[index] = item
	cart.recalculate()
}

func (cart *Cart) recalculate() {
	sum := 0

	for _, item := range cart.Items {
		cart.Qty = item.Qty
		sum += item.Qty * item.Price
	}

	cart.Total = sum
}
