package main

import (
	"fmt"
	"rekabio-shopping-list/cart"
)

func main() {
	cart := cart.Cart{}
	cart.AddItem("Indomie Rogeng", 2, 3200)
	cart.AddItem("Indomie Resub", 4, 2850)
	fmt.Println(cart)

	cart.RemoveItem(1)
	cart.UpdateItem(0, 1, 3100)

	fmt.Println(cart)
}
